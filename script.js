//code below is js
var elements = document.getElementsByClassName('cell');
var selectedCells = [];
for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("click",onClickCell)
    elements[i].index = i;
}
let x = document.getElementsByClassName("game--status")
x[0].innerHTML = "It's X's turn";
var turn = 1;
addSavedGame();
function onClickCell(evt) {
    let i = evt.currentTarget.index
    if (turn % 2 != 0) {
            elements[i].innerHTML = "X";
            selectedCells.push(i)
            turn++;
    } else {
            elements[i].innerHTML = "O";
            selectedCells.push(i)
            turn++;
        } 
    localStorage.setItem("savedCells",JSON.stringify(selectedCells) )
    if (turn % 2 != 0) {
        let x = document.getElementsByClassName("game--status")
        x[0].innerHTML = "It's X's turn";
    } else {
        let y = document.getElementsByClassName("game--status")
        y[0].innerHTML = "It's O's turn";
    }
    elements[i].removeEventListener("click",onClickCell)
}
var resetBtn = document.getElementsByClassName("game--restart");
resetBtn[0].addEventListener("click", function() {
    reset();
})
function reset() {
    turn = 1;
    selectedCells = []
    for (let i = 0; i < elements.length; i++) {
        elements[i].innerHTML = null;
    }
    window.localStorage.removeItem("savedCells")
    let x = document.getElementsByClassName("game--status")
    x[0].innerHTML = "It's X's turn";
    for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("click",onClickCell)
    elements[i].index = i;
}
}
function addSavedGame() {
    if(window.localStorage.getItem("savedCells") != null){
let savedItems = JSON.parse(window.localStorage.getItem("savedCells"))
selectedCells = savedItems
for (let j = 0; j < savedItems.length; j++) {
    let index = savedItems[j]
    elements[index].removeEventListener("click",onClickCell)
 if(j === 0){
    elements[index].innerHTML = "X"
 }else if(j % 2 != 0){
    elements[index].innerHTML = "O"
 }else {
    elements[index].innerHTML = "X" 
 }  
}
if(savedItems.length % 2 != 0){
    turn = 2
    let x = document.getElementsByClassName("game--status")
    x[0].innerHTML = "It's O's turn";
}else {
    turn = 1
    let x = document.getElementsByClassName("game--status")
    x[0].innerHTML = "It's X's turn";
}
}
}
//copyright reserve for shayan hamidi
